package pieces;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PiecesImporterTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final InputStream originalIn = System.in;
    @Mock
    private PieceFactory pieceFactory;

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
        given(pieceFactory.createPiece(eq(PieceType.BISHOP), any(Color.class), any(Location.class)))
                .willAnswer(invocationOnMock ->
                        Optional.of(new Bishop(invocationOnMock.getArgument(1), invocationOnMock.getArgument(2))));
        given(pieceFactory.createPiece(eq(PieceType.NIGHT), any(Color.class), any(Location.class)))
                .willAnswer(invocationOnMock ->
                        Optional.of(new Night(invocationOnMock.getArgument(1), invocationOnMock.getArgument(2))));
    }

    @After
    public void restore() {
        System.setOut(originalOut);
        System.setIn(originalIn);
    }

    @Test
    public void shouldReadZeroPieces() {
        // given
        String numberOfPieces = "0";

        // when
        List<Piece> pieces = getImporterWithInput(numberOfPieces).readPieces();

        // then
        assertThat(outContent.toString().contains("Enter number of pieces:"), is(true));
        assertTrue(pieces.isEmpty());
    }

    @Test
    public void shouldReadSingleBishop() {
        // given
        String numberOfPieces = "1";
        String pieceType = "B";
        String color = "W";
        String position = "C3";

        // when
        List<Piece> pieces = getImporterWithInput(numberOfPieces, color, pieceType, position).readPieces();

        // then
        assertThat(outContent.toString().contains("Piece 1"), is(true));
        assertEquals(1, pieces.size());
        Piece piece = pieces.get(0);
        assertThat(piece.getType(), is(PieceType.BISHOP));
        assertThat(piece.getColor(), is(Color.WHITE));
        Location location = piece.getLocation();
        assertThat(location.getX(), is(2));
        assertThat(location.getY(), is(2));
    }

    @Test
    public void shouldNotCreateUnknownPiece() {
        // given1
        when(pieceFactory.createPiece(any(PieceType.class), any(Color.class), any(Location.class)))
                .thenReturn(Optional.empty());
        String numberOfPieces = "1";
        String pieceType = "B";
        String color = "W";
        String position = "C3";

        // when
        List<Piece> pieces = getImporterWithInput(numberOfPieces, color, pieceType, position).readPieces();

        // then
        assertThat(outContent.toString().contains("Enter number of pieces:"), is(true));
        assertTrue(pieces.isEmpty());
    }

    @Test
    public void shouldReadMultiplePieces() {
        // given1
        String numberOfPieces = "2";
        String pieceTypeOne = "B";
        String colorOne = "W";
        String positionOne = "C3";
        String pieceTypeTwo = "N";
        String colorTwo = "B";
        String positionTwo = "H8";

        // when
        List<Piece> pieces = getImporterWithInput(
                numberOfPieces,
                colorOne,
                pieceTypeOne,
                positionOne,
                colorTwo,
                pieceTypeTwo,
                positionTwo
        ).readPieces();

        // then
        assertEquals(2, pieces.size());
        Piece pieceOne = pieces.get(0);
        assertThat(pieceOne.getType(), is(PieceType.BISHOP));
        assertThat(pieceOne.getColor(), is(Color.WHITE));
        Location locationOne = pieceOne.getLocation();
        assertThat(locationOne.getX(), is(2));
        assertThat(locationOne.getY(), is(2));
        Piece pieceTwo = pieces.get(1);
        assertThat(pieceTwo.getType(), is(PieceType.NIGHT));
        assertThat(pieceTwo.getColor(), is(Color.BLACK));
        Location locationTwo = pieceTwo.getLocation();
        assertThat(locationTwo.getX(), is(7));
        assertThat(locationTwo.getY(), is(7));
    }

    @Test
    public void shouldReadSingleNightWithTypeCorrected() {
        // given
        String numberOfPieces = "1";
        String invalidPieceType = "S";
        String pieceType = "N";
        String color = "W";
        String position = "C3";

        // when
        List<Piece> pieces = getImporterWithInput(
                numberOfPieces,
                color,
                invalidPieceType,
                pieceType,
                position
        ).readPieces();

        // then
        assertThat(outContent.toString().contains("Enter type (B/N):"), is(true));
        assertThat(outContent.toString().contains("That is not correct value"), is(true));
        assertEquals(1, pieces.size());
        Piece piece = pieces.get(0);
        assertThat(piece.getType(), is(PieceType.NIGHT));
        assertThat(piece.getColor(), is(Color.WHITE));
        Location location = piece.getLocation();
        assertThat(location.getX(), is(2));
        assertThat(location.getY(), is(2));
    }

    @Test
    public void shouldReadSingleNightWithColorCorrected() {
        // given
        String numberOfPieces = "1";
        String pieceType = "N";
        String invalidColor = "S";
        String color = "W";
        String position = "C3";

        // when
        List<Piece> pieces = getImporterWithInput(
                numberOfPieces,
                invalidColor,
                color,
                pieceType,
                position
        ).readPieces();

        // then
        assertThat(outContent.toString().contains("Enter color (W/B):"), is(true));
        assertThat(outContent.toString().contains("That is not correct value"), is(true));
        assertEquals(1, pieces.size());
        Piece piece = pieces.get(0);
        assertThat(piece.getType(), is(PieceType.NIGHT));
        assertThat(piece.getColor(), is(Color.WHITE));
        Location location = piece.getLocation();
        assertThat(location.getX(), is(2));
        assertThat(location.getY(), is(2));
    }

    @Test
    public void shouldReadSingleNightWithPositionCorrected() {
        // given
        String numberOfPieces = "1";
        String pieceType = "N";
        String color = "W";
        String invalidPosition = "S";
        String position = "C3";

        // when
        List<Piece> pieces = getImporterWithInput(
                numberOfPieces,
                color,
                pieceType,
                invalidPosition,
                position
        ).readPieces();

        // then
        assertThat(outContent.toString().contains("Enter position:"), is(true));
        assertThat(outContent.toString().contains("That is not correct value"), is(true));
        assertEquals(1, pieces.size());
        Piece piece = pieces.get(0);
        assertThat(piece.getType(), is(PieceType.NIGHT));
        assertThat(piece.getColor(), is(Color.WHITE));
        Location location = piece.getLocation();
        assertThat(location.getX(), is(2));
        assertThat(location.getY(), is(2));
    }

    private PiecesImporter getImporterWithInput(String... input) {
        StringBuilder simulatedUserInput = new StringBuilder();
        for (String in : input) {
            simulatedUserInput.append(in);
            simulatedUserInput.append(System.getProperty("line.separator"));
        }
        ByteArrayInputStream in = new ByteArrayInputStream(simulatedUserInput.toString().getBytes());
        System.setIn(in);
        return new PiecesImporter(new Scanner(in), pieceFactory);
    }

}