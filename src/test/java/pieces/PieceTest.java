package pieces;

import org.junit.Test;
import pieces.*;

public class PieceTest {

    @Test(expected = AssertionError.class)
    public void shouldFailWhenNoColorProvided() {
        // when + then
        new Bishop(null, new Location(1, 1));
    }

    @Test(expected = AssertionError.class)
    public void shouldFailWhenNoLocationProvided() {
        // when + then
        new Bishop(Color.BLACK, null);
    }

    @Test
    public void shouldCreateBishop() {
        // when
        Piece piece = new Bishop(Color.WHITE, new Location(0, 0));

        // then
        assert piece.getColor() == Color.WHITE;
        assert piece.getType() == PieceType.BISHOP;
        assert piece.getLocation().getX() == 0;
        assert piece.getLocation().getY() == 0;
    }

}