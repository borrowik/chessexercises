package pieces;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class PieceFactoryTest {

    private PieceFactory factory = new PieceFactory();

    @Test
    public void shouldCreateBishop() {
        // given
        PieceType type = PieceType.BISHOP;
        Color color = Color.WHITE;
        Location location = new Location(7, 7);

        // when
        Piece piece = factory.createPiece(type, color, location).orElse(null);

        // then
        assertThat(piece, not(nullValue()));
        assertThat(piece.getColor(), is(color));
        assertThat(piece.getLocation(), is(location));
        assertThat(piece.getType(), is(type));
        assertThat(piece, is(instanceOf(Bishop.class)));
    }

    @Test
    public void shouldCreateNight() {
        // given
        PieceType type = PieceType.NIGHT;
        Color color = Color.BLACK;
        Location location = new Location(1, 1);

        // when
        Piece piece = factory.createPiece(type, color, location).orElse(null);

        // then
        assertThat(piece, not(nullValue()));
        assertThat(piece.getColor(), is(color));
        assertThat(piece.getLocation(), is(location));
        assertThat(piece.getType(), is(type));
        assertThat(piece, is(instanceOf(Night.class)));
    }

    @Test
    public void shouldNotCreateQueen() {
        // given
        PieceType type = PieceType.QUEEN;
        Color color = Color.WHITE;
        Location location = new Location(7, 7);

        // when
        Piece piece = factory.createPiece(type, color, location).orElse(null);

        // then
        assertThat(piece, nullValue());
    }

}