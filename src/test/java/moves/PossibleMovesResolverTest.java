package moves;

import org.junit.Before;
import org.junit.Test;
import pieces.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class PossibleMovesResolverTest {

    private Set<Piece> pieces = new HashSet<>();
    private PossibleMovesResolver possibleMovesResolver = new PossibleMovesResolver();

    @Before
    public void setUp() {
        pieces.clear();
    }

    @Test
    public void shouldReturnNoMovesWhenPiecesEmpty() {
        // when
        Map<Piece, Collection<Location>> result = possibleMovesResolver.getPossibleMoves(new HashSet<>());

        // then
        assertThat(result.isEmpty(), is(true));
    }

    @Test
    public void shouldGetNoPossibleMovesForBishopAtA1IsBlocked() {
        // given
        Piece bishopOne = new Bishop(Color.WHITE, new Location(0,0));
        Piece bishopTwo = new Bishop(Color.WHITE, new Location(1,1));
        pieces.add(bishopOne);
        pieces.add(bishopTwo);

        // when
        Map<Piece, Collection<Location>> result = possibleMovesResolver.getPossibleMoves(pieces);

        // then
        assertThat(result.isEmpty(), is(false));
        assertThat(result.containsKey(bishopOne), is(true));
        assertThat(result.get(bishopOne).isEmpty(), is(true));
    }

    @Test
    public void shouldGetPossibleMovesForTwoBishops() {
        // given
        Piece bishopOne = new Bishop(Color.WHITE, new Location(0,0));
        Piece bishopTwo = new Bishop(Color.BLACK, new Location(0,7));
        pieces.add(bishopOne);
        pieces.add(bishopTwo);

        // when
        Map<Piece, Collection<Location>> result = possibleMovesResolver.getPossibleMoves(pieces);

        // then
        assertThat(result.isEmpty(), is(false));
        assertThat(result.containsKey(bishopOne), is(true));
        Collection<Location> bishopOneMoves = result.get(bishopOne);
        assertThat(bishopOneMoves, hasItems(new Location(1,1)));
        Collection<Location> bishopTwoMoves = result.get(bishopTwo);
        assertThat(bishopTwoMoves, hasItems(new Location(1,6)));
    }

    @Test
    public void shouldGetPossibleMovesForWhiteBishopAtG5AndWhiteNightAtF6() {
        // given
        Piece bishop = new Bishop(Color.WHITE, new Location(6,4));
        Piece night = new Night(Color.WHITE, new Location(5,5));
        pieces.add(bishop);
        pieces.add(night);

        // when
        Map<Piece, Collection<Location>> result = possibleMovesResolver.getPossibleMoves(pieces);

        // then
        assertThat(result.isEmpty(), is(false));
        assertThat(result.containsKey(bishop), is(true));
        Collection<Location> bishopMoves = result.get(bishop);
        assertThat(bishopMoves, hasItems(new Location(2,0), new Location(3,1), new Location(4,2)));
        assertThat(bishopMoves, hasItems(new Location(5,3), new Location(7,3), new Location(7,5)));

        Collection<Location> nightMoves = result.get(night);
        assertThat(nightMoves, hasItems(new Location(3,4), new Location(3,6), new Location(4,3)));
        assertThat(nightMoves, hasItems(new Location(4,7), new Location(6,3), new Location(6,7)));
        assertThat(nightMoves, hasItems(new Location(7,4), new Location(7,6)));
    }

}