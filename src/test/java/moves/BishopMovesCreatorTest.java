package moves;

import org.junit.Before;
import org.junit.Test;
import pieces.*;

import java.util.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class BishopMovesCreatorTest {

    private Map<Location, Color> locations = new HashMap<>();
    private BishopMovesCreator bishopMovesCreator = new BishopMovesCreator();

    @Before
    public void setUp() {
        locations.clear();
    }

    @Test
    public void shouldGetPossibleMovesForSingleBishopAtA1() {
        // given
        Piece bishop = new Bishop(Color.WHITE, new Location(0,0));

        // when
        Collection<Location> moves = bishopMovesCreator.possibleMoves(bishop, locations);

        // then
        assertThat(moves.isEmpty(), is(false));
        assertThat(moves, hasItems(
                new Location(1,1),
                new Location(2,2),
                new Location(3,3),
                new Location(4,4),
                new Location(5,5),
                new Location(6,6),
                new Location(7,7)));
    }

    @Test
    public void shouldGetPossibleMovesForSingleBishopAtA8() {
        // given
        Piece bishop = new Bishop(Color.WHITE, new Location(0,7));

        // when
        Collection<Location> moves = bishopMovesCreator.possibleMoves(bishop, locations);

        // then
        assertThat(moves.isEmpty(), is(false));
        assertThat(moves, hasItems(
                new Location(1,6),
                new Location(2,5),
                new Location(3,4),
                new Location(4,3),
                new Location(5,2),
                new Location(6,1),
                new Location(7,0)));
    }

    @Test
    public void shouldGetPossibleMovesForSingleBishopAtD4() {
        // given
        Piece bishop = new Bishop(Color.WHITE, new Location(3,3));

        // when
        Collection<Location> moves = bishopMovesCreator.possibleMoves(bishop, locations);

        // then
        assertThat(moves.isEmpty(), is(false));
        assertThat(moves, hasItems(new Location(0,0), new Location(0, 6)));
        assertThat(moves, hasItems(new Location(1,1), new Location(1, 5)));
        assertThat(moves, hasItems(new Location(2,2), new Location(2, 4)));
        assertThat(moves, hasItems(new Location(4,2), new Location(4, 4)));
        assertThat(moves, hasItems(new Location(5,1), new Location(5, 5)));
        assertThat(moves, hasItems(new Location(6,0), new Location(6, 6)));
        assertThat(moves, hasItems(new Location(7, 7)));
    }

    @Test
    public void shouldGetPossibleMovesForSingleBishopAtG5() {
        // given
        Piece bishop = new Bishop(Color.WHITE, new Location(6,4));

        // when
        Collection<Location> moves = bishopMovesCreator.possibleMoves(bishop, locations);

        // then
        assertThat(moves.isEmpty(), is(false));
        assertThat(moves, hasItems(new Location(2,0), new Location(3, 1)));
        assertThat(moves, hasItems(new Location(3,7), new Location(4, 2)));
        assertThat(moves, hasItems(new Location(4,6), new Location(5, 3)));
        assertThat(moves, hasItems(new Location(5,5), new Location(7, 3)));
        assertThat(moves, hasItems(new Location(7,5)));
    }

    @Test
    public void shouldGetNoPossibleMovesForBishopAtA1WithBlockingNightAtB2() {
        // given
        Piece bishop = new Bishop(Color.WHITE, new Location(0,0));
        Piece night = new Night(Color.WHITE, new Location(1,1));
        locations.put(night.getLocation(), night.getColor());

        // when
        Collection<Location> moves = bishopMovesCreator.possibleMoves(bishop, locations);

        // then
        assertThat(moves.isEmpty(), is(true));
    }

    @Test
    public void shouldGetReducedPossibleMovesForBishopAtG5WithBlockingNightAtF6() {
        // given
        Piece bishop = new Bishop(Color.WHITE, new Location(6,4));
        Piece night = new Night(Color.WHITE, new Location(5,5));
        locations.put(night.getLocation(), night.getColor());

        // when
        Collection<Location> moves = bishopMovesCreator.possibleMoves(bishop, locations);

        // then
        assertThat(moves.isEmpty(), is(false));
        assertThat(moves, not(hasItem(new Location(3, 7))));
        assertThat(moves, not(hasItem(new Location(4, 6))));
        assertThat(moves, not(hasItem(new Location(5, 5))));
        assertThat(moves, hasItems(new Location(2,0), new Location(3, 1)));
        assertThat(moves, hasItems(new Location(4, 2)));
        assertThat(moves, hasItems(new Location(5,3)));
        assertThat(moves, hasItems(new Location(7, 3), new Location(7,5)));
    }

    @Test
    public void shouldSameLocationNotPossibleWhenBishopAddedToOtherLocations() {
        // given
        Piece bishop = new Bishop(Color.WHITE, new Location(3,3));
        locations.put(bishop.getLocation(), bishop.getColor());

        // when
        Collection<Location> moves = bishopMovesCreator.possibleMoves(bishop, locations);

        // then
        assertThat(moves.isEmpty(), is(false));
        assertThat(moves, not(hasItems(new Location(3,3))));
    }

    @Test
    public void shouldCaptureNightForBishopAtA1() {
        // given
        Piece bishop = new Bishop(Color.WHITE, new Location(0,0));
        Piece night = new Night(Color.BLACK, new Location(1,1));
        locations.put(night.getLocation(), night.getColor());

        // when
        Collection<Location> moves = bishopMovesCreator.possibleMoves(bishop, locations);

        // then
        assertThat(moves.isEmpty(), is(false));
        assertThat(moves, hasItem(new Location(1,1)));
    }

}