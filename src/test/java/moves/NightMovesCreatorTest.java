package moves;

import org.junit.Before;
import org.junit.Test;
import pieces.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

public class NightMovesCreatorTest {

    private Map<Location, Color> locations = new HashMap<>();
    private NightMovesCreator nightMovesCreator = new NightMovesCreator();

    @Before
    public void setUp() {
        locations.clear();
    }

    @Test
    public void shouldGetPossibleMovesForSingleNightAtA1() {
        // given
        Piece night = new Night(Color.WHITE, new Location(0,0));

        // when
        Collection<Location> moves = nightMovesCreator.possibleMoves(night, locations);

        // then
        assertThat(moves.isEmpty(), is(false));
        assertThat(moves, hasItems(
                new Location(1,2),
                new Location(2,1)));
    }

    @Test
    public void shouldGetPossibleMovesForSingleNightAtD5() {
        // given
        Piece night = new Night(Color.WHITE, new Location(3,4));

        // when
        Collection<Location> moves = nightMovesCreator.possibleMoves(night, locations);

        // then
        assertThat(moves.isEmpty(), is(false));
        assertThat(moves, hasItems(
                    new Location(1,3),
                new Location(1,5),
                new Location(2,2),
                new Location(2,6),
                new Location(4,2),
                new Location(4,6),
                new Location(5,3),
                new Location(5,5)));
    }

    @Test
    public void shouldGetPossibleMovesForWhiteNightAtD5WithWhiteBishopAtB4() {
        // given
        Piece night = new Night(Color.WHITE, new Location(3,4));
        Piece bishop = new Bishop(Color.WHITE, new Location(1,3));
        locations.put(bishop.getLocation(), bishop.getColor());

        // when
        Collection<Location> moves = nightMovesCreator.possibleMoves(night, locations);

        // then
        assertThat(moves.isEmpty(), is(false));
        assertThat(moves, not(hasItem(new Location(1, 3))));
        assertThat(moves, hasItems(
                new Location(1,5),
                new Location(2,2),
                new Location(2,6),
                new Location(4,2),
                new Location(4,6),
                new Location(5,3),
                new Location(5,5)));
    }

    @Test
    public void shouldCaptureBishopForNightAtD5() {
        // given
        Piece night = new Night(Color.WHITE, new Location(3,4));
        Piece bishop = new Bishop(Color.BLACK, new Location(1,3));
        locations.put(bishop.getLocation(), bishop.getColor());

        // when
        Collection<Location> moves = nightMovesCreator.possibleMoves(night, locations);

        // then
        assertThat(moves.isEmpty(), is(false));
        assertThat(moves, hasItems(new Location(1,3)));
    }

}