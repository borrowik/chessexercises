import moves.PossibleMovesResolver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pieces.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ValidMovesCalculatorTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private ValidMovesCalculator calculator;
    private List<Piece> pieces = new ArrayList<>();
    @Mock
    private PossibleMovesResolver possibleMovesResolver;

    @Captor
    private ArgumentCaptor<Set<Piece>> piecesCapture;

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
        calculator = new ValidMovesCalculator(possibleMovesResolver);
        pieces.clear();
        when(possibleMovesResolver.getPossibleMoves(any())).thenReturn(new HashMap<>());
    }

    @After
    public void restore() {
        System.setOut(originalOut);
    }

    @Test
    public void shouldPrintNoValidMovesWhenNoPieces() {
        // when
        calculator.printValidMoves(new ArrayList<>());

        // then
        String output = outContent.toString();
        assertThat(output.contains("Valid moves"), is(true));
        assertThat(output.contains("No valid moves available"), is(true));
    }

    @Test
    public void shouldFindSingleBishopMovesAtA1() {
        // given
        Location location = new Location(0,0);
        Piece bishop = new Bishop(Color.WHITE, location);
        pieces.add(bishop);
        when(possibleMovesResolver.getPossibleMoves(anySet())).thenReturn(singletonMap(bishop, singletonList(location)));

        // when
        calculator.printValidMoves(pieces);

        // then
        String output = outContent.toString();
        assertThat(output.contains("Valid moves"), is(true));
        assertThat(output.contains("B on a1"), is(true));
    }

    @Test
    public void shouldRejectBishopNotOnBoard() {
        // given
        Piece bishop = new Bishop(Color.WHITE, new Location(-1,-2));
        pieces.add(bishop);

        // when
        calculator.printValidMoves(pieces);

        // then
        String output = outContent.toString();
        assertThat(output.contains("Invalid pieces on board where found"), is(true));
        assertThat(output.contains("No valid moves available"), is(true));
    }

    @Test
    public void shouldRejectPieceOnTheSameLocation() {
        // given
        Location location =new Location(1,1);
        Piece bishop = new Bishop(Color.WHITE, location);
        Piece night = new Night(Color.WHITE, location);
        pieces.add(bishop);
        pieces.add(night);

        // when
        calculator.printValidMoves(pieces);

        // then
        String output = outContent.toString();
        assertThat(output.contains("Invalid pieces on board where found"), is(true));
        verify(possibleMovesResolver).getPossibleMoves(piecesCapture.capture());
        Set<Piece> pieces = piecesCapture.getValue();
        assertThat(pieces.size(), is(1));
        assertThat(pieces, hasItem(bishop));
    }

    @Test
    public void shouldFindBishopWithTwoPossibleMoves() {
        // given
        Location locationOne = new Location(1,1);
        Location locationTwo = new Location(2,2);
        Piece bishop = new Bishop(Color.WHITE, new Location(0,0));
        pieces.add(bishop);
        when(possibleMovesResolver.getPossibleMoves(anySet())).thenReturn(singletonMap(bishop, Arrays.asList(locationTwo, locationOne)));

        // when
        calculator.printValidMoves(pieces);

        // then
        String output = outContent.toString();
        assertThat(output.contains("Valid moves"), is(true));
        assertThat(output.contains("B on a1: [b2, c3]"), is(true));
    }

}