package moves;

import pieces.Color;
import pieces.Location;
import pieces.Piece;
import pieces.PieceType;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.*;

public class PossibleMovesResolver {

    private static final Map<PieceType, ? extends PossibleMovesCreator> MOVE_CREATOR;
    static {
        MOVE_CREATOR = Map.of(PieceType.BISHOP, new BishopMovesCreator(), PieceType.NIGHT, new NightMovesCreator());
    }

    public Map<Piece, Collection<Location>> getPossibleMoves(Set<Piece> pieces) {
        Map<Location, Color> locationToColorMap = pieces.stream()
                .collect(toUnmodifiableMap(Piece::getLocation, Piece::getColor));
        Map<Piece, Collection<Location>> possibleMoves = new HashMap<>();
        pieces.forEach(piece -> {
                    Collection<Location> moves = collectMoves(piece, locationToColorMap);
                    possibleMoves.put(piece, moves);
                }
        );
        return possibleMoves;
    }

    private Collection<Location> collectMoves(Piece piece, Map<Location, Color> locationToColorMap) {
        return getPossibleMoveCreator(piece.getType())
                .possibleMoves(piece, locationToColorMap);
    }

    private PossibleMovesCreator getPossibleMoveCreator(PieceType pieceType) {
        if (MOVE_CREATOR.containsKey(pieceType)) {
            return MOVE_CREATOR.get(pieceType);
        } else {
            throw new UnsupportedOperationException(
                    "Possible moves creator for piece type:" + pieceType + " is not supported, yet");
        }
    }
}
