package moves;

import pieces.Color;
import pieces.Location;
import pieces.Piece;

import java.util.*;

import static java.util.Collections.emptyList;

class BishopMovesCreator implements PossibleMovesCreator {

    private static Location[] vectors = new Location[]{
            new Location(1,1), new Location(-1,-1),
            new Location(-1,1), new Location(1,-1),
    };

    @Override
    public Collection<Location> possibleMoves(Piece piece, Map<Location, Color> locationToColor) {
        Location pieceLocation = piece.getLocation();
        Map<Location, Color> existingLocationToColor = getLocationsFiltered(locationToColor, pieceLocation);
        List<Location> possible = new ArrayList<>();
        for (Location vector : vectors) {
            movable(pieceLocation, vector, possible, existingLocationToColor, piece.getColor());
        }
        return possible;
    }

    private Collection<Location> movable(Location location, Location vector, Collection<Location> possible,
                                         Map<Location, Color> existingLocationToColor, Color color) {
        Location nextLocation = new Location(location.getX() + vector.getX(), location.getY() + vector.getY());
        if (!isOnBoard(nextLocation)) {
            return emptyList();
        } else {
                if (!existingLocationToColor.containsKey(nextLocation)) {
                    possible.add(nextLocation);
                    return movable(nextLocation, vector, possible, existingLocationToColor, color);
                }
                if (isCapturable(color, existingLocationToColor.get(nextLocation))) {
                    possible.add(nextLocation);
                 }
                return emptyList();
        }
    }

    private boolean isCapturable(Color color, Color existingColor) {
        return !color.equals(existingColor);
    }

}
