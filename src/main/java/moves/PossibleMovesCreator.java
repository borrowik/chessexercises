package moves;

import pieces.Color;
import pieces.Location;
import pieces.Piece;

import java.util.Collection;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

public interface PossibleMovesCreator {

       int BOARD_SIZE = 8;

       Collection<Location> possibleMoves(Piece piece, Map<Location, Color> locationToColor);

       default Map<Location, Color> getLocationsFiltered(Map<Location, Color> locationToColorMap, Location exceptLocation) {
              return locationToColorMap.entrySet().stream()
                      .filter(entry -> !exceptLocation.equals(entry.getKey()))
                      .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
       }

       default boolean isOnBoard(Location location) {
              return location.getX() >= 0 && location.getY() >= 0 && location.getX() < BOARD_SIZE && location.getY() < BOARD_SIZE;
       }

}
