package moves;

import pieces.Color;
import pieces.Location;
import pieces.Piece;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

class NightMovesCreator implements PossibleMovesCreator {

    private static Location[] vectors = new Location[]{
            new Location(2,1), new Location(2,-1), new Location(-2,1), new Location(-2,-1),
            new Location(1,2), new Location(1,-2), new Location(-1,2), new Location(-1,-2),
    };

    @Override
    public Collection<Location> possibleMoves(Piece piece, Map<Location, Color> locationToColor) {
        Location current = piece.getLocation();
        Map<Location, Color> existingLocationToColor = getLocationsFiltered(locationToColor, current);
        return stream(vectors)
                .map(vector -> movable(current, vector, existingLocationToColor, piece.getColor()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    private Optional<Location> movable(Location pieceLocation, Location vector, Map<Location, Color> existingLocationToColor, Color color) {
        return Optional.of(new Location(pieceLocation.getX() + vector.getX(), pieceLocation.getY() + vector.getY()))
                .filter(this::isOnBoard)
                .filter(location ->  isCapturable(location, color, existingLocationToColor));
    }

    private boolean isCapturable(Location nextLocation, Color color, Map<Location, Color> existingLocationToColor) {
        return !existingLocationToColor.containsKey(nextLocation) || !color.equals(existingLocationToColor.get(nextLocation));
    }

}
