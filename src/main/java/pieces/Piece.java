package pieces;

import java.util.Objects;

public abstract class Piece {

    private Color color;
    private PieceType type;
    private Location location;

    public Piece(PieceType type, Color color, Location location) {
        assert type!= null;
        assert color!= null;
        assert location!= null;
        this.color = color;
        this.type = type;
        this.location = location;
    }

    public Color getColor() {
        return color;
    }

    public PieceType getType() {
        return type;
    }

    public Location getLocation() {
        return location;
    }

    public abstract String getLabel();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Piece piece = (Piece) o;
        return color == piece.color &&
                type == piece.type &&
                location.equals(piece.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, type, location);
    }
}
