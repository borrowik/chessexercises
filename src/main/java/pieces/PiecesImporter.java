package pieces;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class PiecesImporter {

    private Scanner input;
    private PieceFactory pieceFactory;

    public PiecesImporter(Scanner input, PieceFactory pieceFactory) {
        assert input != null;
        assert pieceFactory != null;
        this.input = input;
        this.pieceFactory = pieceFactory;
    }

    public List<Piece> readPieces() {
        int numberOfPieces = getNumberOfPieces();
        List<Piece> pieces = new ArrayList<>(numberOfPieces);
        for (int i = 1; i <= numberOfPieces; i++) {
            System.out.println("Piece " + i);
            Color color = getColor();
            PieceType type = getType();
            Location location = getLocation();
            pieceFactory.createPiece(type, color, location).ifPresent(pieces::add);
        }
        return pieces;
    }

    private String readSingleValue(Pattern pattern) {
        String value = input.next();
        while (!pattern.matcher(value).matches()) {
            System.out.println("That is not correct value");
            value = input.next();
        }
        return value.toLowerCase();
    }

    private Location getLocation() {
        System.out.println("Enter position:");
        String value = readSingleValue(Pattern.compile("[A-Ha-h][1-8]"));
        return new Location(value.charAt(0) - 97, value.charAt(1) - 49);
    }

    private PieceType getType() {
        System.out.println("Enter type (B/N):");
        String value = readSingleValue(Pattern.compile("[NnBb]"));
        return "b".equals(value) ? PieceType.BISHOP : PieceType.NIGHT;
    }

    private Color getColor() {
        System.out.println("Enter color (W/B):");
        String value = readSingleValue(Pattern.compile("[WwBb]"));
        return "w".equals(value) ? Color.WHITE : Color.BLACK;
    }

    private int getNumberOfPieces() {
        System.out.println("Enter number of pieces:");
        return input.nextInt();
    }
}
