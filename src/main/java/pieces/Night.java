package pieces;

public class Night extends Piece {

    public Night(Color color, Location location) {
        super(PieceType.NIGHT, color, location);
    }

    @Override
    public String getLabel() {
        return "N";
    }

}
