package pieces;

import java.util.Optional;

public class PieceFactory {

    Optional<Piece> createPiece(PieceType type, Color color, Location location) {
        Piece piece = null;
        if(PieceType.BISHOP.equals(type)){
            piece = new Bishop(color, location);
        } else if(PieceType.NIGHT.equals(type)){
            piece = new Night(color, location);
        }
        return Optional.ofNullable(piece);
    }

}
