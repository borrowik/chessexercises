package pieces;

public class Bishop extends Piece {

    public Bishop(Color color, Location location) {
        super(PieceType.BISHOP, color, location);
    }

    @Override
    public String getLabel() {
        return "B";
    }
}
