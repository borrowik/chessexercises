package pieces;

import pieces.Location;
import pieces.Piece;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class PieceValidator {

    private PieceValidator() {
    }

    public static Set<Piece> getPiecesOnBoard(List<Piece> pieces) {
        Set<Location> locations = new HashSet<>();
        return pieces.stream()
                .filter(locationOnBoard())
                .filter(p -> !locations.contains(p.getLocation()))
                .map(addPieceLocation(locations))
                .collect(Collectors.toSet());
    }

    private static Function<Piece, Piece> addPieceLocation(Set<Location> locations) {
        return piece -> {
            locations.add(piece.getLocation());
            return piece;
        };
    }

    private static Predicate<Piece> locationOnBoard() {
        return piece ->
                piece.getLocation().getX() >= 0 &&
                        piece.getLocation().getX() < 8 &&
                        piece.getLocation().getY() >= 0 &&
                        piece.getLocation().getY() < 8;
    }
}
