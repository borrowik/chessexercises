import moves.PossibleMovesResolver;
import pieces.Piece;
import pieces.PieceFactory;
import pieces.PiecesImporter;

import java.util.List;
import java.util.Scanner;

public class BishopAndNightsChess {

    public static void main(String[] args) {
        System.out.println("FindValidMoves");
        Scanner input = new Scanner(System.in);
        PiecesImporter importer = new PiecesImporter(input, new PieceFactory());
        ValidMovesCalculator calculator = new ValidMovesCalculator(new PossibleMovesResolver());
        do {
            List<Piece> pieces =  importer.readPieces();
            calculator.printValidMoves(pieces);
            System.out.println("Continue (Y/N)?:");
        } while ("y".equals(input.next().toLowerCase()));
    }

}
