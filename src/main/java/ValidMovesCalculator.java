import moves.PossibleMovesResolver;
import pieces.Location;
import pieces.Piece;
import pieces.PieceValidator;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.joining;

class ValidMovesCalculator {

    private PossibleMovesResolver possibleMovesResolver;

    ValidMovesCalculator(PossibleMovesResolver possibleMovesResolver) {
        this.possibleMovesResolver = possibleMovesResolver;
    }

    void printValidMoves(List<Piece> pieces) {
        System.out.println("Valid moves");
        Set<Piece> validPieces = getValidPieces(pieces);
        if(validPieces.isEmpty()) {
            System.out.println("No valid moves available");
            return;
        }
        possibleMovesResolver.getPossibleMoves(validPieces).entrySet().stream()
                .map(entry -> getPossibleLocations(entry.getKey(), entry.getValue()))
                .forEach(System.out::println);
    }

    private Set<Piece> getValidPieces(List<Piece> pieces) {
        Set<Piece> validPieces = PieceValidator.getPiecesOnBoard(pieces);
        if (validPieces.size() != pieces.size()) {
            System.out.println("Invalid pieces on board where found");
        }
        return validPieces;
    }

    private String getPossibleLocations(Piece piece, Collection<Location> locations) {
        String allLocations = locations.stream()
                .map(Location::toString)
                .sorted(Comparator.naturalOrder())
                .collect(joining(", "));
        return piece.getLabel() + " on " + piece.getLocation() + ": [" + allLocations + "]";
    }

}
